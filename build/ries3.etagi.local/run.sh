#/bin/bash
/etc/init.d/php5-fpm start;
/etc/init.d/nginx start ;
/etc/init.d/ssh start;
/etc/init.d/sendmail start;
cd /var/hosting/ries3.etagi.com/www/;
php composer.phar install && php composer.phar update;
chown -R www-data /var/hosting/ries3.etagi.com/www/;
tailf /dev/null