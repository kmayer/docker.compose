#!/bin/bash
#RESOLUTIONS="920x920 800x800 600x600 400x400 200x200"
RESOLUTIONS_SERVER="10241280 1024768 10281024 127230 255172 300267 320240 520390 640480 800600 960720 1280960 19201440 22401680 25601920"
#RESOLUTIONS_SERVER="19201440"
SOURCE="logo_1200.png"
SOURCE_UA="logo_1200_ua.png"
SOURCE_RED="logo_red.png"
#SOURCE_RED_UA="logo_1200_red_ua.png"
rm -r logo_new
mkdir logo_new
echo "/usr/bin/convert new ver logo"
rm -r with-logo
mkdir with-logo
cd with-logo
for RESOLUTION in $RESOLUTIONS_SERVER
do
wget --output-document $RESOLUTION.jpg http://api-media.etagi.com/$RESOLUTION/photos/59084829976cf.jpg
wget --output-document ${RESOLUTION}-ua.jpg http://api-media.novoe.od.ua/$RESOLUTION/photos/591c0e7662ea4.jpg
#wget --output-document $RESOLUTION-big.jpg http://api-media.etagi.com/$RESOLUTION/photos/OIyZOUc.jpg
#wget --output-document $RESOLUTION-big-ua.jpg http://api-media.novoe.od.ua/$RESOLUTION/photos/OIyZOUc.jpg
done
gimp *.jpg
# gimp -a -