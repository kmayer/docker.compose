#/bin/bash
/etc/init.d/php5-fpm start;
/etc/init.d/nginx start ;
/etc/init.d/ssh start;
cd /var/hosting/developers.etagi.com/www;
mkdir /var/hosting/developers.etagi.com/www/runtime;
chmod 777 -R -v  /var/hosting/developers.etagi.com/www/runtime;
cd /var/hosting/developers.etagi.com/www;
php composer.phar install;
php composer.phar self-update;
php composer.phar update fxp/composer-asset-plugin;
php composer.phar update;
tailf /dev/null